import Vue from 'vue';
import VueI18n from 'vue-i18n';
import en from './locales/en.json';
import de from './locales/de.json';
import ps from './locales/pseudo/ps.json';

const debug = process.env.NODE_ENV !== 'production';
Vue.use(VueI18n);

const locale = 'en';
// The fallbackLocale is set to empty for now so that we can see what
// has been translated and what hasn't
const fallbackLocale = '';
const messages: VueI18n.LocaleMessages = {
  en,
  de,
};

// Add the pseudo language when only if debug is true
if (debug) {
  const key = 'ps';
  messages[key] = ps;
}

const i18n = new VueI18n({
  locale,
  fallbackLocale,
  messages,
});

export default i18n;

