/* eslint-disable */
import $ from 'jquery';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LPUtils = function () {
  function LPUtils() {
    _classCallCheck(this, LPUtils);
  }

  _createClass(LPUtils, null, [{
    key: "getDomain",
    value: function getDomain(account, name) {
      var domains = account.startsWith("le") ? "hc1n.dev.lprnd.net" : "adminlogin.liveperson.net";
      return new Promise(function (res, rej) {
        return $.ajax({
          url: "https://" + domains + "/csdr/account/" + account + "/service/" + name + "/baseURI.lpCsds?version=1.0",
          jsonp: "cb",
          jsonpCallback: "domainCallback",
          cache: true,
          dataType: "jsonp",
          success: function success(data) {
            return res(data.ResultSet.lpData[0].lpServer);
          },
          error: function error(e, text) {
            return rej(text);
          }
        });
      });
    }
  }, {
    key: "agentProfile",
    value: function agentProfile(account, agentID) {
      var _this = this;

      return new Promise(function (res, rej) {
        return _this.getDomain(account, "acCdnDomain").then(function (accdnDomain) {
          return $.ajax({
            url: "https://" + accdnDomain + "/api/account/" + account + "/configuration/le-users/users/" + agentID,
            jsonp: "cb",
            jsonpCallback: "apCallback",
            cache: true,
            dataType: "jsonp",
            success: function success(accdnResp) {
              return res(accdnResp);
            }
          });
        });
      });
    }
  }, {
    key: "signup",
    value: function signup(account) {
      var _this2 = this;

      return new Promise(function (res, rej) {
        return _this2.getDomain(account, "idp").then(function (idpDomain) {
          return $.ajax({
            url: "https://" + idpDomain + "/api/account/" + account + "/signup.jsonp",
            jsonp: "callback",
            dataType: "jsonp",
            success: function success(idpResp) {
              return res(idpResp.jwt);
            }
          });
        });
      });
    }
  }, {
    key: "getJWT",


    // fetch jwt from localstorage or create one
    value: function getJWT(account) {
      var localJWT = localStorage.getItem(account + "-jwt");
      if (localJWT) return Promise.resolve(localJWT);else return this.signup(account).then(function (newJWT) {
        localStorage.setItem(account + "-jwt", newJWT);
        return Promise.resolve(newJWT);
      });
    }
  }, {
    key: "clearJWT",
    value: function clearJWT(account) {
      localStorage.removeItem(account + "-jwt");
    }
  }, {
    key: "getIdFromJWT",
    value: function getIdFromJWT(jwt) {
      return JSON.parse(atob(jwt.split('.')[1])).sub;
    }
  }]);

  return LPUtils;
}();

var LPWs = function () {
  _createClass(LPWs, null, [{
    key: "connect",
    value: function connect(url) {
      return new LPWs(url)._connect();
    }
  }, {
    key: "connectDebug",
    value: function connectDebug(url) {
      return new LPWs(url, true)._connect();
    }
  }]);

  function LPWs(url, debug) {
    _classCallCheck(this, LPWs);

    this.reqs = {};
    this.subs = [];
    this.url = url;
    this.debug = debug;
  }

  _createClass(LPWs, [{
    key: "_connect",
    value: function _connect() {
      var _this3 = this;

      return new Promise(function (resolve, reject) {
        var ws = new WebSocket(_this3.url);
        _this3.ws = ws;
        ws.onopen = function () {
          return resolve(_this3);
        };
        ws.onmessage = function (msg) {
          return _this3.onmessage(msg);
        };
        ws.onclose = function (evt) {
          _this3.ws = null;
          reject(evt);
        };
      });
    }
  }, {
    key: "request",
    value: function request(type, body, headers) {
      var _this4 = this;

      return new Promise(function (resolve, reject) {
        var obj = {
          "kind": "req",
          "type": type,
          "body": body || {},
          "id": Math.floor(Math.random() * 1e9),
          "headers": headers
        };
        _this4.reqs[obj.id] = function (type, code, body) {
          return resolve({
            type: type,
            code: code,
            body: body
          });
        };
        var str = JSON.stringify(obj);
        if (_this4.debug) console.log("sending: " + str);
        _this4.ws.send(str);
      });
    }
  }, {
    key: "onNotification",
    value: function onNotification(filterFunc, _onNotification) {
      this.subs.push({
        filter: filterFunc,
        cb: _onNotification
      });
    }
  }, {
    key: "toFuncName",
    value: function toFuncName(reqType) {
      var str = reqType.substr(1 + reqType.lastIndexOf('.'));
      return str.charAt(0).toLowerCase() + str.slice(1);
    }
  }, {
    key: "registerRequests",
    value: function registerRequests(arr) {
      var _this5 = this;

      arr.forEach(function (reqType) {
        return _this5[_this5.toFuncName(reqType)] = function (body, headers) {
          return _this5.request(reqType, body, headers);
        };
      });
    }
  }, {
    key: "onmessage",
    value: function onmessage(msg) {
      if (this.debug) console.log("recieved: " + msg.data);
      var obj = JSON.parse(msg.data);
      if (obj.kind == "resp") {
        var id = obj.reqId;
        delete obj.reqId;
        delete obj.kind;
        this.reqs[id].call(this, obj.type, obj.code, obj.body);
        delete this.reqs[id];
      } else if (obj.kind == "notification") {
        this.subs.forEach(function (sub) {
          if (sub.filter.call(this, obj)) {
            sub.cb.call(this, obj.body);
          };
        });
      }
    }
  }]);

  return LPWs;
}();


export {
  LPUtils,
  LPWs
}
