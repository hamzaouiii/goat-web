let loggedIn = false;
const requireAuth =  (to: any , from: any, next: any) => {
  if (!loggedIn) {
    next({
      path: '/login',
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
};
const isLoggedIn = (value: boolean ) => {
  loggedIn = value;
};
export default {
  isLoggedIn,
  requireAuth,
  loggedIn,
};
