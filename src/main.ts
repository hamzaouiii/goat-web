import Vue from 'vue';
import App from './App.vue';
import i18n from './localization';
import router from './router';
import store from './store';
import './registerServiceWorker';
import moment from 'moment';
import 'moment/locale/de';

Vue.config.productionTip = false;

Vue.filter('time', (timestamp: string) => {
  moment.locale(navigator.language);
  return moment(timestamp).format('LT');
});

export default new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
