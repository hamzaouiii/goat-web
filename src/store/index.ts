import Vue from 'vue';
import Vuex from 'vuex';
import chatPlugin from './chat';
import LocalizationStore from './localization';
import LoginStore from './login';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    LocalizationStore,
    LoginStore,
  },
  strict: debug,
  plugins: [chatPlugin({
    accountId: '4756526',
  })],
});
