const state = {
  authenticated: false,
};
const getters = {
  authenticated: (state: any): boolean => state.authenticated,
};
const actions = {
  async setAuthentication({ commit }: Context, auth: boolean) {
    commit('SET_AUTHENTICATION', auth);
  },
};
const mutations = {
  SET_AUTHENTICATION(state: any, auth: boolean): void {
      state.authenticated = auth;
    },
};
export default {
  state,
  getters,
  actions,
  mutations,
};
