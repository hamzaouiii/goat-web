interface Message {
  sender: string;
  text: string;
  username: string;
  isSent: boolean;
  isReceived: boolean;
  timestamp: number;
}

interface MessageBody {
  dialogId: string;
  originatorId?: string;
  serverTimestamp?: number;
  event: {
    contentType: string;
    message: string;
    type: string;
  };
}

interface State {
  userId: string;
  messages: Message[];
  openConversationId: string;
  agentIsTyping: boolean;
  agentIsActive: boolean;
  unreadMessageCount: number;
}

interface Mutation {
  type: string;
  payload: any;
}

interface Store {
  state: State;
  dispatch: Dispatch;
  subscribe: any;
  registerModule: registerModule;
}

interface Context {
  commit: Commit;
  state: State;
  dispatch: object;
}

// TODO: Define the socket better
interface Socket {
  registerRequests: any;
  initConnection: any;
  onNotification: any;
  subscribeExConversations: any;
  subscribeMessagingEvents: any;
  consumerRequestConversation: any;
  publishEvent: any;
}

interface MessagingEvent {
  event: {
    type: string;
    chatState: string;
  };
}

interface SubscriptionEvent {
  result: {
    convId: string;
  };
}

type Commit = (type: string, obj: any) => void;
type Dispatch = (type: string, obj: any) => void;
type registerModule = (moduleName: string, obj: any) => void;
