// tslint:disable-next-line
const { LPUtils, LPWs } = require('../../lib/LivePerson');

const state: State = {
  userId: '',
  messages: [],
  openConversationId: '',
  agentIsTyping: false,
  agentIsActive: false,
  unreadMessageCount: 0,
};

const getters = {
  messages: (state: State): Message[] => state.messages,
  isAgentTyping: (state: State): boolean => state.agentIsTyping,
  isAgentActive: (state: State): boolean => state.agentIsActive,
  getMessageCount: (state: State): number => state.unreadMessageCount,
};

const actions = {
  addUserId({ commit }: Context, id: string): void {
    commit('USER_ID', id);
  },
  addOpenConversation({ commit }: Context, openConversation: {convId: string}): void {
    commit('ADD_CONVERSATION', openConversation.convId);
  },
  markChatState({ commit }: Context, chatState: string): void {
    switch (chatState) {
      case 'COMPOSING':
        commit('AGENT_TYPING', true);
        commit('AGENT_ACTIVE', true);
        break;
      case 'ACTIVE':
        commit('AGENT_TYPING', false);
        commit('AGENT_ACTIVE', true);
        break;
      default:
        commit('AGENT_TYPING', false);
    }
  },
  messageReceived({ commit, state }: Context, messageBody: MessageBody): void {
    const sender = messageBody.originatorId === state.userId ? 'user' : 'agent';
    const message = {
      sender,
      username: sender,
      text: messageBody.event.message,
      isSent: sender === 'user',
      isReceived: sender === 'agent',
      timestamp: messageBody.serverTimestamp,
    };
    commit('ADD_MESSAGE', message);
  },
  sendMessage({ commit, state }: Context, messageString: string) {
    const messageBody: MessageBody = {
      dialogId: state.openConversationId,
      event: {
        type: 'ContentEvent',
        contentType: 'text/plain',
        message: messageString,
      },
    };
    commit('SEND_MESSAGE', messageBody);
  },
  addNotification({commit, state}: Context) {
    commit('ADD_MESSAGE_UNREAD', state);
  },
  resetNotification({commit, state}: Context) {
    commit('RESET_MESSAGE_UNREAD', state);
  },
};

const mutations = {
  USER_ID(state: State, id: string): void {
    state.userId = id;
  },
  ADD_CONVERSATION(state: State, conversationId: string): void {
    state.openConversationId = conversationId;
  },
  AGENT_TYPING(state: State, isTyping: boolean): void {
    state.agentIsTyping = isTyping;
  },
  AGENT_ACTIVE(state: State, isActive: boolean): void {
    state.agentIsActive = isActive;
  },
  ADD_MESSAGE(state: State, message: Message): void {
    state.messages = [...state.messages, message];
  },
  SEND_MESSAGE() {
    // This method is a placeholder, look at the plugin to see where it invoked
  },
  ADD_MESSAGE_UNREAD(state: State) {
    state.unreadMessageCount++;
  },
  RESET_MESSAGE_UNREAD(state: State) {
    state.unreadMessageCount = 0;
  },
};


function withSubscriptionID(subscriptionID: string) {
  return function(notification: {body: {subscriptionId: string}}) {
    return notification.body.subscriptionId === subscriptionID;
  };
}

function withType(type: string) {
  return function(notification: {type: {includes: (type: string) => string}}) {
    return notification.type.includes(type);
  };
}

// These are all the events that we will be subscribing to from the LivePerson socket
const apiRequestTypes: string[] = [
  'cqm.SubscribeExConversations', 'ms.PublishEvent',
  'cm.ConsumerRequestConversation', 'ms.SubscribeMessagingEvents',
  'InitConnection', 'cm.UpdateConversationField',
];

function handleOpenedSocket(store: Store, {socket, jwt}: {socket: Socket, jwt: string}) {
  socket.registerRequests(apiRequestTypes);
  const me = LPUtils.getIdFromJWT(jwt);
  store.dispatch('addUserId', me);
  socket.initConnection({}, [{
    type: '.ams.headers.ConsumerAuthentication',
    jwt,
  }]);

  socket.onNotification(withType('MessagingEvent'), (resp: {changes: MessagingEvent[]}) => {
    resp.changes.forEach((change: MessagingEvent) => {
      if (change.event.type === 'ContentEvent') {
        store.dispatch('messageReceived', change);
        store.dispatch('addNotification', change);
      } else if (change.event.type === 'ChatStateEvent') {
        store.dispatch('markChatState', change.event.chatState);
      }
    });
  });

  socket.subscribeExConversations({
    convState: ['OPEN'],
  }).then((resp: {body: {subscriptionId: string}}) => {
    socket.onNotification(
      withSubscriptionID(resp.body.subscriptionId),
      function(notificationBody: {changes: SubscriptionEvent[]}) {
        notificationBody.changes.forEach((change) => {
          store.dispatch('addOpenConversation', change.result);
          socket.subscribeMessagingEvents({
            fromSeq: 0,
            dialogId: change.result.convId,
          });
        });
      },
    );

    // Force a new message
    // TODO: Figure out what to do here!
    if (!store.state.openConversationId) {
      socket.consumerRequestConversation();
    }
    store.subscribe((mutation: Mutation): void => {
      switch (mutation.type) {
        case 'SEND_MESSAGE':
          socket.publishEvent(mutation.payload);
      }
    });
  });
  // keeping the connection alive every 30s
  setInterval(function() {
  socket.publishEvent({message: 'ping'});
  }, 30000);
}

export default function livePersonPlugin({ accountId }: { accountId: string}) {
  return (store: Store) => {
    LPUtils.getJWT(accountId).then((jwt: string) => {
      LPUtils.getDomain(accountId, 'asyncMessagingEnt').then(
        (umsDomain: string) => {
          const socketURL = 'wss://' + umsDomain + '/ws_api/account/' + accountId + '/messaging/consumer?v=3';
          LPWs.connect(socketURL).then((socket: Socket) => handleOpenedSocket(store, {socket, jwt}));
        },
      );
    });
    store.registerModule('livePerson', {
      state,
      mutations,
      getters,
      actions,
    });
  };
}
