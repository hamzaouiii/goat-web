import App from '@/main';

const state: LocalizationState = {
  selectedLanguage: 'en',
  availableLanguages: [{
    code: 'en',
    name: 'English',
  }, {
    code: 'de',
    name: 'Deutsch',
  }],
};
const actions = {
  async setLanguage({ commit }: Context, language: string) {
    // Over here we wait for the app to load completely
    await require('@/main');
    commit('SET_LANGUAGE', language);
  },
};

const mutations = {
  SET_LANGUAGE(state: LocalizationState, language: string): void {
    if (language in App.$i18n.messages) {
      state.selectedLanguage = language;
      App.$i18n.locale = language;
    }
  },
};
const getters = {
  getLanguage: (state: LocalizationState): string => state.selectedLanguage,
};
export default {
  state,
  actions,
  mutations,
  getters,
};
