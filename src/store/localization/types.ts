interface LocalizationState {
  selectedLanguage: string;
  availableLanguages: Array<{
    code: string;
    name: string;
  }>;
}
