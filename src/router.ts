import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import Login from './views/Login.vue';
import auth from '@/lib/Login/';


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: auth.requireAuth,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      beforeEnter: auth.requireAuth,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
  ],
});
