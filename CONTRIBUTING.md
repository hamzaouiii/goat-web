# Contribution to goat-web
When contributing to this repository, please do the changes on your local and create a **Pull Request**. 

## Pull Request Process
1. Commit your changes on your local into a new branch.
2. Make sure that Master is merged into your branch.
3. Push the branch to remote and create the Pull Request.

## Coding Style

### Indentation 

We use spaces with 2 indents instead of tabs.

### Naming Convention

#### Source File name

Every view, component, test or style file should be named using CamelCase, that is to use an uppercase as a start for every word of the name.
###### Example
`message-list.vue` should be named `MessageList.vue`.

#### CSS Classes
For CSS classes it is recommended to use the [BEM](http://getbem.com/naming/) Naming Convention. Simply deviding the classes to blocks, elements and modifiers.
