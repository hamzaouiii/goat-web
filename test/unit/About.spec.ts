import Vue from 'vue';
import { shallow } from '@vue/test-utils';
import About from '@/views/About.vue';
import VueI18n from 'vue-i18n';

// Setup internationalization
Vue.use(VueI18n);
const i18n = new VueI18n({});

describe('About.vue', () => {
  it('renders', () => {
    const msg = 'about.message';
    const wrapper = shallow(About, {i18n});
    expect(wrapper.text()).toMatch(msg);
  });
});
