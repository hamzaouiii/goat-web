# goat-web
Web app project for GOAT 

![Project badge](https://img.shields.io/badge/project-goat-blue.svg "Project badge")
![Build Status](https://travis-ci.com/crvshlab/goat-web.svg?token=Mn6CBV28BnzEZxicxEWZ&branch=master "Travis")

## Getting Started

### Prerequisites

You will need `yarn` and `node` globally installed before starting.

You can install Yarn through the [Homebrew](https://brew.sh/) package manager. This will also install Node.js if it is not already installed.

```bash
brew install yarn
```
>**_Note_**: if you are not running Mac or Linux and cannot get Homebrew, check the yarn [install](https://yarnpkg.com/lang/en/docs/install/) docs for more details.

### Installing

To install the dependencies simply enter the `yarn` command in your terminal.

```bash
yarn 
```

### Running on local

Run the following command to run the app on your local:

```bash
yarn serve
```

### Building for production

To build for production run:

```bash
yarn build
```
### Linting

For linitng run:
```bash
yarn lint
```

### Localization

For localization, we use the [vue-i18n](https://github.com/kazupon/vue-i18n) library.
The localization files live in `src/localization/locale/` directory.

We also support pseudo localization when running in debug mode. To generate the pseudolocalized
locale file you can run the following command.
```bash
yarn pseudolocalize
```

It will create a file in `src/localized/locale/pseudo/` directory.

To view the app in the pseudo locale, you can pass in a query parameter to activate that locale.
```
?lang=ps
```


## Running the tests

For running unit tests run the following command:
```bash
yarn test
```
For e2e tests please run:
```bash
yarn e2e
```
To open the GUI of [cypress](https://www.cypress.io/) please enter:
```bash
yarn e2e:open
```
## Deployment
Goat-web is not being deployed yet.

## Contributing
Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on the process of submitting code to this repo.

## Continuous Integration
We use Travis CI for automatically building the app, and running:
 - [linting rules](tslint.json)
 - [unit tests](/test/unit)
 - [integration tests](/test/e2e/specs)

## Licence

Copyright © crvsh GmbH. All rights reserved.
